﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

	GameObject screenFader;

	void Awake () {
		screenFader = GameObject.FindGameObjectWithTag (Tags.fader);
	}

	public void LoadByIndex () {
		print ("click");

		Invoke ("LoadScene", 1.5f);

		screenFader.GetComponent<SceneFaderInOut> ().FadeToBlack ();

	}

	void LoadScene () {
		SceneManager.LoadScene ("TheClimb");
	}
}
