﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPlayerDetection : MonoBehaviour {

	private GameObject player;
	private LastPlayerSighting lastPlayersighting;

	void Awake () {
		player = GameObject.FindGameObjectWithTag (Tags.player);
		lastPlayersighting = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<LastPlayerSighting> ();
	}

	void OnTriggerStay(Collider other) {
		if (GetComponent<Renderer>().enabled) {
			if (other.gameObject == player) {
				lastPlayersighting.position = other.transform.position;
			}
		}
	}
}
