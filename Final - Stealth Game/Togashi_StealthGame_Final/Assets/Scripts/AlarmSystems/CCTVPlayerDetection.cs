﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCTVPlayerDetection : MonoBehaviour {

	private GameObject player;
	private LastPlayerSighting lastPlayersighting;

	void Awake () {
		player = GameObject.FindGameObjectWithTag (Tags.player);
		lastPlayersighting = GameObject.FindGameObjectWithTag (Tags.gameController).GetComponent<LastPlayerSighting> ();
	}

	void OnTriggerStay(Collider other) {
		if (other.gameObject == player) {
			Vector3 relPlayerPos = player.transform.position - transform.position;
			RaycastHit hit;

			if (Physics.Raycast (transform.position, relPlayerPos, out hit)) {
				if(hit.collider.gameObject == player) {
					lastPlayersighting.position = player.transform.position;
				}
			}
		}
	}
}
