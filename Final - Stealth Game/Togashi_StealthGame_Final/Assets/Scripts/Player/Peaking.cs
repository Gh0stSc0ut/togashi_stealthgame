﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peaking : MonoBehaviour {

	public float peakDistanceRight = 1.5f;
	public float peakDistanceLeft = -1.5f;

	private GameObject mainCamera;
	private Vector3 resetCamera;
	private PlayerMovement playerMovement;
	private CameraFollow cameraMovement;
	private GameObject cameraFollow;

	void Awake () {
		mainCamera = GameObject.FindGameObjectWithTag (Tags.camera);
		playerMovement = GameObject.FindGameObjectWithTag (Tags.player).GetComponent<PlayerMovement> ();
		cameraMovement = GameObject.FindGameObjectWithTag (Tags.camBase).GetComponent<CameraFollow> ();
		cameraFollow = GameObject.FindGameObjectWithTag (Tags.camFollow);

		resetCamera = mainCamera.transform.position;
	}

	void Update () {
		float peakInputVal = Input.GetAxis ("LeftPeak");
		if (Input.GetButtonDown ("LeftPeak")) {
			resetCamera = cameraFollow.transform.position;
			Vector3 finalPosLeft = resetCamera + cameraFollow.transform.right * peakDistanceLeft;
			cameraFollow.transform.position = finalPosLeft;
			//cameraFollow.transform.position = new Vector3 (cameraFollow.transform.position.x, cameraFollow.transform.position.y, cameraFollow.transform.position.z + peakDistance);
			//cameraFollow.transform.position = cameraFollow.transform.position - Vector3.forward * peakDistance;

			playerMovement.enabled = false;
			//cameraMovement.enabled = false;

		} else if (Input.GetButtonDown ("RightPeak")) {
			resetCamera = cameraFollow.transform.position;
			Vector3 finalPosRight = resetCamera + cameraFollow.transform.right * peakDistanceRight;
			cameraFollow.transform.position = finalPosRight;
			//cameraFollow.transform.position = new Vector3 (cameraFollow.transform.position.x, cameraFollow.transform.position.y, cameraFollow.transform.position.z - peakDistance);
			//cameraFollow.transform.position = cameraFollow.transform.position + Vector3.forward * peakDistance;

			playerMovement.enabled = false;
			//cameraMovement.enabled = false;
		} 

		if (Input.GetButtonUp ("LeftPeak")) {
			cameraFollow.transform.position = resetCamera;

			playerMovement.enabled = true;
			//cameraMovement.enabled = true;

		} else if (Input.GetButtonUp ("RightPeak")) {
			cameraFollow.transform.position = resetCamera;

			playerMovement.enabled = true;
			//cameraMovement.enabled = true;
		}
	}
}
