﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting_Test : MonoBehaviour {

	public float damage = 70f;
	public float range = 100f;

	private GameObject player;
	private EnemyHealth enemeyHealth;

	void Awake () {
		player = GameObject.FindGameObjectWithTag (Tags.player);
		enemeyHealth = GameObject.FindGameObjectWithTag (Tags.enemy).GetComponent<EnemyHealth> ();
	}

	void Update () {
		if (Input.GetButtonDown ("Fire")) {
			print ("Fire");
			Shoot ();
		}
	}

	void Shoot () {
		RaycastHit hit;
		if (Physics.Raycast (player.transform.position, player.transform.forward, out hit, range)) {
			EnemyHealth target = hit.transform.GetComponent<EnemyHealth> ();
			if (target != null) {
				enemeyHealth.TakeDamage (damage);
			}
		}
	}
}
