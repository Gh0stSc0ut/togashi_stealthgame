﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

	public float maximumDamage = 120f;
	public float minimumDamage = 45f;
	public float damage = 50f;

	public AudioClip shotClip;
	public float flashIntensity = 3f;
	public float fadeSpeed = 10f;
	public Animator anim;
	public float range = 50f;
	Vector3 shotHitPoint;
	public Transform gunTip;
	public GameObject handGun;
	public GameObject waistGun;

	private HashIDs hash;
	private LineRenderer laserShotLine;
	private Light laserShotLight;
	private SphereCollider col;
	private Transform enemy;
	private GameObject enemyPar;
	private EnemyHealth enemyHealth;
	private bool shooting;
	private float scaledDamage;

	void Awake () {
		laserShotLine = GetComponentInChildren<LineRenderer>();
		laserShotLight = laserShotLine.gameObject.GetComponent<Light>();
		col = GetComponent<SphereCollider>();
		enemy = GameObject.FindGameObjectWithTag(Tags.enemy).transform;
		enemyPar = GameObject.FindGameObjectWithTag (Tags.enemyParent);
		enemyHealth = enemy.gameObject.GetComponent<EnemyHealth>();
		hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();

		laserShotLine.enabled = false;
		laserShotLight.intensity = 0f;
		scaledDamage = maximumDamage - minimumDamage;
		handGun.SetActive (false);
	}

	void Update () {
		float shot = anim.GetFloat (hash.shotFloat);
		if (Input.GetButton ("Fire")) {
			anim.SetBool ("Shooting", true);
			if (shot > 0.2f && !shooting) {
				Shoot ();
			}

			if (shot < 0.2f) {
				shooting = false;
				laserShotLine.enabled = false;
				handGun.SetActive (false);
				waistGun.SetActive (true);
			}
			laserShotLight.intensity = Mathf.Lerp (laserShotLight.intensity, 0f, fadeSpeed * Time.deltaTime);
		} else {
			shooting = false;
			anim.SetBool ("Shooting", false);
		}
		if (anim.GetBool ("Shooting") == false) {
			handGun.SetActive (false);
			waistGun.SetActive (true);
		} 

		if (anim.GetBool ("Shooting") == true) {
			handGun.SetActive (true);
			waistGun.SetActive (false);
		}
	}

	void OnAnimatorIK (int layerIndex) {
		float aimWeight = anim.GetFloat(hash.aimWeightFloat);
		anim.SetIKPosition (AvatarIKGoal.RightHand, transform.forward * 100f);
		anim.SetIKPositionWeight(AvatarIKGoal.RightHand, aimWeight);
	}

	void Shoot () {

		RaycastHit hit;
		if (Physics.Raycast (gunTip.position, transform.forward, out hit, range)) {
			shotHitPoint = hit.point;
			EnemyHealth target = hit.transform.GetComponent<EnemyHealth> ();
			if (target != null) {
				target.TakeDamage (damage);
			}
		}

		/*shooting = true;
		float fractionalDistance = (col.radius - Vector3.Distance (transform.position, enemy.position)) / col.radius;
		float damage = scaledDamage * fractionalDistance + minimumDamage;
		enemyHealth.TakeDamage (damage);*/
		ShotEffects();
	}

	void ShotEffects() {
		laserShotLine.SetPosition(0, laserShotLine.transform.position);
		laserShotLine.SetPosition(1, shotHitPoint);
		laserShotLine.enabled = true;
		laserShotLight.intensity = flashIntensity;
		AudioSource.PlayClipAtPoint(shotClip, laserShotLight.transform.position);
	}
}
